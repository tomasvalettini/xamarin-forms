﻿using System;

namespace Greetings
{
	public interface IPlatformInfo
	{
		string GetModel();
		string GetVersion();
	}
}