﻿using System;

namespace Greetings
{
	public interface IPlatformSoundPlayer
	{
		void PlaySound(int samplingRate, byte[] pcmData);
	}
}

