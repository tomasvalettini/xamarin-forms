﻿using System;
using Xamarin.FormsBook.Toolkit;
using Xamarin.Forms;
using System.Windows.Input;

namespace Greetings
{
	public class PowersViewModel : ViewModelBase
	{
		double exponent, power;

		public double BaseValue { private set; get; }

		public double Exponent
		{
			private set
			{
				if (SetProperty(ref exponent, value))
				{
					Power = Math.Pow(BaseValue, exponent);
				}
			}

			get
			{
				return exponent;
			}
		}

		public double Power
		{
			private set { SetProperty(ref power, value); }
			get { return power; }
		}

		public ICommand IncreaseExponentCommand { private set; get; }
		public ICommand DecreaseExponentCommand { private set; get; }

		public PowersViewModel(double baseValue)
		{
			//Initialize properties
			BaseValue = baseValue;
			Exponent = 0;

			// Initialize ICommand properties
			IncreaseExponentCommand = new Command(() => 
				{
					Exponent += 1;
				});
			DecreaseExponentCommand = new Command(() => 
				{
					Exponent -= 1;
				});
		}
	}
}