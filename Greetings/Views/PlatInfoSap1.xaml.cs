﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Greetings
{
	public partial class PlatInfoSap1 : ContentPage
	{
		public PlatInfoSap1 ()
		{
			InitializeComponent ();

			IPlatformInfo platformInfo = DependencyService.Get<IPlatformInfo> ();
			modelLabel.Text = platformInfo.GetModel ();
			versionLabel.Text = platformInfo.GetVersion ();
		}
	}
}