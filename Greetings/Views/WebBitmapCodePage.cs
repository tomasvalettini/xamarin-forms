﻿using System;

using Xamarin.Forms;

namespace Greetings {
	public class WebBitmapCodePage : ContentPage {
		public WebBitmapCodePage () {
			//method1();
			method2();
			//method3();
		}

		private void method1() {
			string uri = "https://developer.xamarin.com/demo/IMG_1415.JPG";

			Content = new Image {
				Source = ImageSource.FromUri(new Uri(uri))
			};
		}

		private void method2() {
			Content = new Image {
				Source = "https://developer.xamarin.com/demo/IMG_1415.JPG?width"
			};
		}

		private void method3() {
			Content = new Image {
				Source = new UriImageSource {
					Uri = new Uri("https://developer.xamarin.com/demo/IMG_1415.JPG")
				}
			};
		}
	}
}