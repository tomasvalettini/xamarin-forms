﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Greetings
{
	public partial class DynamicVsStaticPage : ContentPage
	{
		public DynamicVsStaticPage ()
		{
			InitializeComponent ();

			Device.StartTimer (TimeSpan.FromSeconds(1), 
				() => {
					Resources["currentDateTime"] = DateTime.Now.ToString();
					return true;
				});
		}
	}
}

