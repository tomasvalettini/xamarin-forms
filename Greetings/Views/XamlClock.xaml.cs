﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Greetings
{
	public partial class XamlClock : ContentPage
	{
		public XamlClock ()
		{
			InitializeComponent ();
			Device.StartTimer (TimeSpan.FromSeconds (1), onTimerTick);
		}

		bool onTimerTick ()
		{
			DateTime dt = DateTime.Now;
			timeLabel.Text = dt.ToString ("T");
			dateLabel.Text = dt.ToString ("D");
			return true;
		}
	}
}
