﻿using System;
using Xamarin.Forms;

namespace Greetings {
	public class AltLabel : Label {
		public static readonly BindableProperty PointSizeProperty =
			BindableProperty.Create("PointSize",	// propertyName
									typeof(double),
									typeof(AltLabel),
									8.0,
									propertyChanged: OnPointSizeChanged);

		public double PointSize {
			set { SetValue(PointSizeProperty, value); }
			get { return (double)GetValue(PointSizeProperty); }
		}

		public AltLabel () {
			SetLabelFontSize ((double) PointSizeProperty.DefaultValue);
		}

		static void OnPointSizeChanged(BindableObject bindable, object oldValue, object newValue) {
			((AltLabel)bindable).OnPointSizeChanged((double)oldValue, (double)newValue);
		}

		void OnPointSizeChanged(double oldValue, double newValue) {
			SetLabelFontSize(newValue);
		}

		void SetLabelFontSize(double pointSize) {
			FontSize = 160 * pointSize / 72;
		}
	}
}