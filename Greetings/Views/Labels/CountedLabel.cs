﻿using System;
using Xamarin.Forms;
using System.ComponentModel;

namespace Greetings {
	public class CountedLabel : Label {
		static readonly BindablePropertyKey WordCountKey =
			BindableProperty.CreateReadOnly("WordCount",
											typeof(int),
											typeof(CountedLabel),
											0);

		public static readonly BindableProperty WordCountProperty = WordCountKey.BindableProperty;

		public int WordCount {
			private set { SetValue(WordCountKey, value); }
			get { return (int)GetValue (WordCountProperty); }
		}
		public CountedLabel() {
			// Set the WordCount property when the Text property changes.
			PropertyChanged += (object sender, PropertyChangedEventArgs args) => {
				if (args.PropertyName == "Text") {
					if (String.IsNullOrEmpty(Text)) {
						WordCount = 0;
					}
					else {
						WordCount = Text.Split(' ', '-', '\u2014').Length;
					}
				}
			};
		}
	}
}