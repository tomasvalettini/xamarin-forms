﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Greetings {
	public partial class BaskervillesCountPage : ContentPage {
		public BaskervillesCountPage () {
			InitializeComponent ();

			int wordCount = countedLabel.WordCount;
			wordCountLabel.Text = wordCount + " words";
		}
	}
}