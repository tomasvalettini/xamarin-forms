﻿using System;

using Xamarin.Forms;

namespace Greetings
{
	public class ColorViewListPage : ContentPage
	{
		public ColorViewListPage ()
		{
			Content = new StackLayout { 
				Children = {
					new Label { Text = "Hello ContentPage" }
				}
			};
		}
	}
}