﻿using System;
using Xamarin.Forms;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Greetings
{
	public class GreetingsPage : ContentPage
	{
		private string msg = "Greetings, Xamarin.Forms!";
		private Label label;
		private StackLayout loggerLayout;
		private Button addButton, removeButton;
		Label displayLabel;
		Button backspaceButton;

		private struct FontCalc
		{
			public FontCalc(Label label, double fontSize, double containerWidth)
				: this()
			{
				// Save the font size.
				FontSize = fontSize;
				// Recalculate the Label height.
				label.FontSize = fontSize;
				SizeRequest sizeRequest =
					label.GetSizeRequest(containerWidth, Double.PositiveInfinity);
				// Save that height.
				TextHeight = sizeRequest.Request.Height;
			}
			public double FontSize { private set; get; }
			public double TextHeight { private set; get; }
		}

		public GreetingsPage ()
		{
			/*
			 * Chapter 2
			 */
			//solutionOne();
			//solutionThree();
			//solutionFour();
			//solutionFive();

			/*
			 * Chapter 3
			 */
			//wrappingParagraphs();
			//textAnsBackgroundColors();
			//textAnsBackgroundColors2();
			//fontSizesAndAttributes();
			//formattedText();
			//formattedText2();
			//formattedParagraph();
			//namedFontSizes();

			/*
			 * Chapter 4
			 */
			//stacks();
			//reflectionStack();
			//reflectionStackExp1();
			//reflectionStackExp2();
			//verticalOptionsDemo();
			//framedText();
			//framedText2();
			//framedText3();
			//sizedBoxView();
			//sizedBoxViewCenter();
			//sizedBoxViewCenter2();
			//colorBlocks();
			//blackCat();

			/*
			 * Chapter 5
			 */
			//whatSize();
			//fontSizes ();
			//estimatedFontSize();
			//fitToSizeClock();
			//accessibilityTest();
			//empiricalFontSize();

			/*
			 * Chapter 6
			 */
			//buttonLogger();
			//twoButtons();
			//buttonLambdas();
			simplestKeypad();

			/*
			 * Chapter 7
			 */

			/*
			 * Chapter 8
			 */

			/*
			 * Chapter 9
			 */
		}

		private void solutionOne ()
		{
			Content = new Label {
				Text = msg
			};
			Padding = new Thickness (0, 20, 0, 0); 
		}

		private void solutionTwo ()
		{
			// not application since the project is not SAP
		}

		private void solutionThree ()
		{
			Content = new Label {
				Text = msg
			};
			Device.OnPlatform (iOS: () => {
				Padding = new Thickness (0, 20, 0, 0);
			});
		}

		private void solutionFour ()
		{
			Content = new Label {
				Text = msg,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
		}

		private void solutionFive ()
		{
			Content = new Label {
				Text = msg,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center
			};
		}

		private void wrappingParagraphs ()
		{
			Content = new Label {
				VerticalOptions = LayoutOptions.Center,
				Text = 
					"Mr. Sherlock Holmes, who was usually very late in " +
				"the mornings, save upon those not infrequent " +
				"occasions when he was up all night, was seated at " +
				"the breakfast table. I stood upon the hearth-rug " +
				"and picked up the stick which our visitor had left " +
				"behind him the night before. It was a fine, thick " +
				"piece of wood, bulbous-headed, of the sort which " +
				"is known as a \u201CPenang lawyer.\u201D Just " +
				"under the head was a broad silver band, nearly an " +
				"inch across, \u201CTo James Mortimer, M.R.C.S., " +
				"from his friends of the C.C.H.,\u201D was engraved " +
				"upon it, with the date \u201C1884.\u201D It was " +
				"just such a stick as the old-fashioned family " +
				"practitioner used to carry\u2014dignified, solid, " +
				"and reassuring."
			};

			Padding = new Thickness (5, Device.OnPlatform (20, 5, 5), 5, 5);
		}

		private void textAnsBackgroundColors ()
		{
			Content = new Label {
				Text = msg,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				BackgroundColor = Color.Yellow,
				TextColor = Color.Blue
			};
		}

		private void textAnsBackgroundColors2 ()
		{
			Content = new Label {
				Text = msg,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Yellow,
				TextColor = Color.Blue
			};
		}

		private void fontSizesAndAttributes ()
		{
			Content = new Label {
				Text = msg,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold | FontAttributes.Italic
			};
		}

		private void formattedText ()
		{
			FormattedString fs = new FormattedString ();

			fs.Spans.Add (new Span {
				Text = "I "
			});
			fs.Spans.Add (new Span {
				Text = "love",
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold
			});
			fs.Spans.Add (new Span {
				Text = " Xamarin.Forms!"
			});

			Content = new Label {
				FormattedText = fs,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label))
			};
		}

		private void formattedText2 ()
		{
			Content = new Label {
				FormattedText = new FormattedString {
					Spans = {
						new Span {
							Text = "I "
						},
						new Span {
							Text = " love ",
							FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
							FontAttributes = FontAttributes.Bold
						},
						new Span {
							Text = " Xamarin.Forms!"
						}
					}
				},

				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label))
			};
		}

		private void formattedParagraph ()
		{
			Content = new Label {
				FormattedText = new FormattedString {
					Spans = {
						new Span {
							Text = "\u2003There was nothing so "
						},
						new Span {
							Text = "very ",
							FontAttributes = FontAttributes.Italic
						},
						new Span {
							Text = "remarkable in that; nor did Alice " +
							"think it so "
						},
						new Span {
							Text = "very ",
							FontAttributes = FontAttributes.Italic
						},
						new Span {
							Text = "much out of the way to hear the " +
							"Rabbit say to itself \u2018Oh " +
							"dear! Oh dear! I shall be too late!" +
							"\u2019 (when she thought it over " +
							"afterwards, it occurred to her that " +
							"she ought to have wondered at this, " +
							"but at the time it all seemed quite " +
							"natural); but, when the Rabbit actually "
						},
						new Span {
							Text = "took a watch out of its waistcoat-pocket",
							FontAttributes = FontAttributes.Italic
						},
						new Span {
							Text = ", and looked at it, and then hurried on, " +
							"Alice started to her feet, for it flashed " +
							"across her mind that she had never before " +
							"seen a rabbit with either a waistcoat-" +
							"pocket, or a watch to take out of it, " +
							"and, burning with curiosity, she ran " +
							"across the field after it, and was just " +
							"in time to see it pop down a large " +
							"rabbit-hold under the hedge."
						}
					}
				},

				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
			};
		}

		private void namedFontSizes ()
		{
			FormattedString fs = new FormattedString ();
			NamedSize[] ns = {
				NamedSize.Default, NamedSize.Micro, NamedSize.Small,
				NamedSize.Medium, NamedSize.Large
			};

			foreach (NamedSize namedSize in ns) {
				double fontSize = Device.GetNamedSize (namedSize, typeof(Label));

				fs.Spans.Add (new Span {
					Text = String.Format ("Name Size = {0} ({1:F2})", namedSize, fontSize),
					FontSize = fontSize
				});

				if (namedSize != ns [ns.Length - 1]) {
					fs.Spans.Add (new Span {
						Text = Environment.NewLine + Environment.NewLine
					});
				}
			}

			Content = new Label {
				FormattedText = fs,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
		}

		private void stacks ()
		{
			var colors = new[] {
				new { value = Color.White, name = "White" },
				new { value = Color.Silver, name = "Silver" },
				new { value = Color.Gray, name = "Gray" },
				new { value = Color.Black, name = "Black" },
				new { value = Color.Red, name = "Red" },
				new { value = Color.Maroon, name = "Maroon" },
				new { value = Color.Yellow, name = "Yellow" },
				new { value = Color.Olive, name = "Olive" },
				new { value = Color.Lime, name = "Lime" },
				new { value = Color.Green, name = "Green" },
				new { value = Color.Aqua, name = "Aqua" },
				new { value = Color.Teal, name = "Teal" },
				new { value = Color.Blue, name = "Blue" },
				new { value = Color.Navy, name = "Navy" },
				new { value = Color.Pink, name = "Pink" },
				new { value = Color.Fuchsia, name = "Fuchsia" },
				new { value = Color.Purple, name = "Purple" }
			};

			StackLayout stackLayout = new StackLayout {
				Spacing = 0
			};

			foreach (var color in colors) {
				stackLayout.Children.Add (
					new Label {
						Text = color.name,
						TextColor = color.value,
						FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label))
					});
			}

			Padding = new Thickness (5, Device.OnPlatform (20, 5, 5), 5, 5);
			Content = stackLayout;
		}

		private void reflectionStack ()
		{
			StackLayout stackLayout = new StackLayout ();

			foreach (FieldInfo info in typeof(Color).GetRuntimeFields()) {
				if (info.GetCustomAttribute<ObsoleteAttribute> () != null)
					continue;

				if (info.IsPublic && info.IsStatic && info.FieldType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Field: {0}", info.Name)));
				}
			}

			foreach (PropertyInfo info in typeof(Color).GetRuntimeProperties()) {
				MethodInfo methodInfo = info.GetMethod;

				if (methodInfo.IsPublic && methodInfo.IsStatic && methodInfo.ReturnType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Property: {0}", info.Name)));
				}
			}

			Padding = new Thickness (5, Device.OnPlatform (20, 5, 5), 5, 5);
			Content = new ScrollView {
				Content = stackLayout
			};
		}

		private void reflectionStackExp1 ()
		{
			StackLayout stackLayout = new StackLayout {
				BackgroundColor = Color.Blue,
				HorizontalOptions = LayoutOptions.Start
			};

			foreach (FieldInfo info in typeof(Color).GetRuntimeFields()) {
				if (info.GetCustomAttribute<ObsoleteAttribute> () != null)
					continue;

				if (info.IsPublic && info.IsStatic && info.FieldType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Field: {0}", info.Name)));
				}
			}

			foreach (PropertyInfo info in typeof(Color).GetRuntimeProperties()) {
				MethodInfo methodInfo = info.GetMethod;

				if (methodInfo.IsPublic && methodInfo.IsStatic && methodInfo.ReturnType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Property: {0}", info.Name)));
				}
			}

			Padding = new Thickness (5, Device.OnPlatform (20, 5, 5), 5, 5);
			Content = new ScrollView {
				Content = stackLayout,
				BackgroundColor = Color.Red
			};
		}

		private void reflectionStackExp2 ()
		{
			StackLayout stackLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal
			};

			foreach (FieldInfo info in typeof(Color).GetRuntimeFields()) {
				if (info.GetCustomAttribute<ObsoleteAttribute> () != null)
					continue;

				if (info.IsPublic && info.IsStatic && info.FieldType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Field: {0}", info.Name)));
				}
			}

			foreach (PropertyInfo info in typeof(Color).GetRuntimeProperties()) {
				MethodInfo methodInfo = info.GetMethod;

				if (methodInfo.IsPublic && methodInfo.IsStatic && methodInfo.ReturnType == typeof(Color)) {
					stackLayout.Children.Add (createColorLabel ((Color)info.GetValue (null), String.Format ("Property: {0}", info.Name)));
				}
			}

			Padding = new Thickness (5, Device.OnPlatform (20, 5, 5), 5, 5);
			Content = new ScrollView {
				Orientation = ScrollOrientation.Horizontal,
				Content = stackLayout
			};
		}

		private Label createColorLabel (Color color, string name)
		{
			Color backgroundColor = Color.Default;

			if (color != Color.Default) {
				double luminance = 0.30 * color.R +
				                   0.59 * color.G +
				                   0.11 * color.B;
				backgroundColor = luminance > 0.5 ? Color.Black : Color.White;
			}

			return new Label {
				Text = name,
				TextColor = color,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				BackgroundColor = backgroundColor
			};
		}

		private void verticalOptionsDemo ()
		{
			Color[] colors = { Color.Yellow, Color.Blue };
			int flipFlopper = 0;

			// Create the Labels sorted by LayoutAlignment property.
			IEnumerable<Label> labels = 
				from field in typeof(LayoutOptions).GetRuntimeFields ()
				where field.IsPublic && field.IsStatic
				orderby ((LayoutOptions)field.GetValue(null)).Alignment
				select new Label {
					Text = "VerticalOptions = " + field.Name,
					VerticalOptions = (LayoutOptions)field.GetValue (null),
					XAlign = TextAlignment.Center,
					FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
					TextColor = colors [flipFlopper],
					BackgroundColor = colors [flipFlopper = 1 - flipFlopper]
				};
			// transfer to StackLayout
			StackLayout stackLayout = new StackLayout ();

			foreach (Label label in labels) {
				stackLayout.Children.Add (label);
			}

			Padding = new Thickness (0, Device.OnPlatform (20, 0, 0), 0, 0);
			Content = stackLayout;
		}

		private void framedText ()
		{
			Padding = new Thickness (20);
			Content = new Frame {
				OutlineColor = Color.Accent,
				Content = new Label {
					Text = "I've been framed!",
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center
				},
				HasShadow = false
			};
		}

		private void framedText2 ()
		{
			Padding = new Thickness (20);
			Content = new Frame {
				OutlineColor = Color.Accent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Content = new Label {
					Text = "I've been framed!",
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label))
				},
				HasShadow = false
			};
		}

		private void framedText3 ()
		{
			BackgroundColor = Color.Aqua;
			Padding = new Thickness (20);
			Content = new Frame {
				OutlineColor = Color.Black,
				BackgroundColor = Color.Yellow,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				Content = new Label {
					Text = "I've been framed!",
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
					FontAttributes = FontAttributes.Italic,
					TextColor = Color.Blue
				},
				HasShadow = false
			};
		}

		private void sizedBoxView ()
		{
			Content = new BoxView {
				Color = Color.Accent
			};
		}

		private void sizedBoxViewCenter ()
		{
			Content = new BoxView {
				Color = Color.Accent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
		}

		private void sizedBoxViewCenter2 ()
		{
			BackgroundColor = Color.Pink;
			Content = new BoxView {
				Color = Color.Navy,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 200,
				HeightRequest = 100
			};
		}

		private void colorBlocks ()
		{
			StackLayout stackLayout = new StackLayout {
				HorizontalOptions = LayoutOptions.Center
			};

			foreach (FieldInfo info in typeof(Color).GetRuntimeFields()) {
				if (info.GetCustomAttribute<ObsoleteAttribute> () != null)
					continue;

				if (info.IsPublic && info.IsStatic && info.FieldType == typeof(Color)) {
					stackLayout.Children.Add (createColorView ((Color)info.GetValue (null), info.Name));
				}
			}

			foreach (PropertyInfo info in typeof(Color).GetRuntimeProperties()) {
				MethodInfo methodInfo = info.GetMethod;

				if (methodInfo.IsPublic && methodInfo.IsStatic && methodInfo.ReturnType == typeof(Color)) {
					stackLayout.Children.Add (createColorView ((Color)info.GetValue (null), info.Name));
				}
			}

			Padding = new Thickness (0, Device.OnPlatform (20, 5, 5), 0, 5);
			Content = new ScrollView {
				Content = stackLayout,
			};
		}

		private View createColorView (Color color, string name)
		{
			return new Frame {
				OutlineColor = Color.Accent,
				Padding = new Thickness (5),
				HasShadow = false,
				Content = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Spacing = 15,
					Children = {
						new BoxView {
							Color = color
						},
						new Label {
							Text = name,
							FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
							FontAttributes = FontAttributes.Bold,
							VerticalOptions = LayoutOptions.Center,
							HorizontalOptions = LayoutOptions.StartAndExpand
						},
						new StackLayout {
							Children = {
								new Label {
									Text = String.Format ("{0:X2}-{1:X2}-{2:X2}",
										(int)(255 * color.R),
										(int)(255 * color.G),
										(int)(255 * color.B)),
									VerticalOptions = LayoutOptions.CenterAndExpand,
									IsVisible = color != Color.Default
								},
								new Label {
									Text = String.Format ("{0:F2}, {1:F2}, {2:F2}",
										color.Hue,
										color.Saturation,
										color.Luminosity),
									VerticalOptions = LayoutOptions.CenterAndExpand,
									IsVisible = color != Color.Default
								}
							},
							HorizontalOptions = LayoutOptions.End
						}
					}
				}
			};
		}

		private void blackCat ()
		{
			StackLayout mainStack = new StackLayout ();
			StackLayout textStack = new StackLayout {
				Padding = new Thickness (5),
				Spacing = 10
			};

			//Get access to the text resourse.
			Assembly assembly = GetType ().GetTypeInfo ().Assembly;
			string resource = "Greetings.Texts.BlackCat.txt";

			using (Stream stream = assembly.GetManifestResourceStream (resource)) {
				using (StreamReader reader = new StreamReader (stream)) {
					bool gotTitle = false;
					string line;

					// Read in a line (which is actually a paragraph).
					while (null != (line = reader.ReadLine ())) {
						Label label = new Label {
							Text = line,
							// Black text for ebooks!
							TextColor = Color.Black
						};
						if (!gotTitle) {
							// Add first label (the title) to mainStack.
							label.HorizontalOptions = LayoutOptions.Center;
							label.FontSize = Device.GetNamedSize (NamedSize.Medium, label);
							label.FontAttributes = FontAttributes.Bold;
							mainStack.Children.Add (label);
							gotTitle = true;
						} else {
							// Add subsequent labels to textStack.
							textStack.Children.Add (label);
						}
					}
				}
			}

			// Put the textStack in a ScrollView with FillAndExpand.
			ScrollView scrollView = new ScrollView {
				Content = textStack,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness (5, 0),
			};
			// Add the ScrollView as a second child of mainStack.
			mainStack.Children.Add (scrollView);
			// Set page content to mainStack.
			Content = mainStack;
			// White background for ebooks!
			BackgroundColor = Color.White;

			// Add some iOS padding for the page.
			Padding = new Thickness (0, Device.OnPlatform (20, 0, 0), 0, 0);
		}

		private void whatSize ()
		{
			label = new Label {
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			Content = label;
			SizeChanged += OnPageSizeChanged;
		}

		void OnPageSizeChanged (object sender, EventArgs args)
		{
			label.Text = String.Format ("{0} \u00D7 {1}", Width, Height);
		}

		private void fontSizes ()
		{
			BackgroundColor = Color.White;
			StackLayout stackLayout = new StackLayout {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			// Do the NamedSize values.
			NamedSize[] namedSizes = {
				NamedSize.Default, NamedSize.Micro, NamedSize.Small,
				NamedSize.Medium, NamedSize.Large
			};

			foreach (NamedSize namedSize in namedSizes) {
				double fontSize = Device.GetNamedSize(namedSize, typeof(Label));
				stackLayout.Children.Add(new Label {
					Text = String.Format("Named Size = {0} ({1:F2})", namedSize, fontSize),
					FontSize = fontSize,
					TextColor = Color.Black
				});
			}

			// Resolution in device-independent units per inch
			double resolution = 160;

			// Draw horizontal separator line.
			stackLayout.Children.Add(new BoxView {
				Color = Color.Accent,
				HeightRequest = resolution / 80
			});

			// Do some numeric point sizes.
			int[] ptSizes = { 4, 6, 8, 10, 12 };

			foreach (double ptSize in ptSizes)
			{
				double fontSize = resolution * ptSize / 72;
				stackLayout.Children.Add(new Label
					{
						Text = String.Format("Point Size = {0} ({1:F2})",
							ptSize, fontSize),
						FontSize = fontSize,
						TextColor = Color.Black
					});
			}
			Content = stackLayout;
		}

		private void estimatedFontSize ()
		{
			label = new Label();
			Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 0);
			ContentView contentView = new ContentView
			{
				Content = label
			};
			contentView.SizeChanged += OnContentViewSizeChanged;
			Content = contentView;
		}

		void OnContentViewSizeChanged (object sender, EventArgs args)
		{
			string text =
				"A default system font with a font size of S " +
				"has a line height of about ({0:F1} * S) and an " +
				"average character width of about ({1:F1} * S). " +
				"On this page, which has a width of {2:F0} and a " +
				"height of {3:F0}, a font size of ?1 should " +
				"comfortably render the ??2 characters in this " +
				"paragraph with ?3 lines and about ?4 characters " +
				"per line. Does it work?";

			// Get View whose size is changing.
			View view = (View)sender;

			// Define two values as multiples of font size.
			double lineHeight = Device.OnPlatform(1.2, 1.2, 1.3);
			double charWidth = 0.5;

			// Format the text and get its character length.
			text = String.Format(text, lineHeight, charWidth, view.Width, view.Height);
			int charCount = text.Length;

			// Because:
			// lineCount = view.Height / (lineHeight * fontSize)
			// charsPerLine = view.Width / (charWidth * fontSize)
			// charCount = lineCount * charsPerLine
			// Hence, solving for fontSize:
			int fontSize = (int)Math.Sqrt(view.Width * view.Height /
				(charCount * lineHeight * charWidth));
			
			// Now these values can be calculated.
			int lineCount = (int)(view.Height / (lineHeight * fontSize));
			int charsPerLine = (int)(view.Width / (charWidth * fontSize));

			// Replace the placeholders with the values.
			text = text.Replace("?1", fontSize.ToString());
			text = text.Replace("??2", charCount.ToString());
			text = text.Replace("?3", lineCount.ToString());
			text = text.Replace("?4", charsPerLine.ToString());

			// Set the Label properties.
			label.Text = text;
			label.FontSize = fontSize;
		}

		private void fitToSizeClock ()
		{
			Label clockLabel = new Label {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			Content = clockLabel;

			// Handle the SizeChanged event for the page.
			SizeChanged += (object sender, EventArgs args) => {
				// Scale the font size to the page width (based on 11 charaters in the displayed string).
				if (this.Width > 0) {
					clockLabel.FontSize = this.Width / 6;
				}
			};

			// Start the timer going.
			Device.StartTimer(TimeSpan.FromSeconds(1), () => {
				//Set the Text property of the Label.
				clockLabel.Text = DateTime.Now.ToString("h:mm:ss tt");
				return true;
			});
		}

		private void accessibilityTest ()
		{
			Label testLabel = new Label {
				Text = "FontSize of 20" + Environment.NewLine + "20 characters across",
				FontSize = 20,
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			Label displayLabel = new Label {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			testLabel.SizeChanged += (sender, args) => {
				displayLabel.Text = String.Format("{0:F0} \u00D7 {1:F0}", testLabel.Width,
					testLabel.Height);
			};

			Content = new StackLayout {
				Children = {
					testLabel,
					displayLabel
				}
			};
		}

		private void empiricalFontSize ()
		{
			label = new Label ();
			Padding = new Thickness (0, Device.OnPlatform (20, 0, 0), 0, 0);

			ContentView contentView = new ContentView {
				Content = label
			};
			contentView.SizeChanged += (object sender, EventArgs args) => {
				// Get View whose size is changing.
				View view = (View)sender;
				if (view.Width <= 0 || view.Height <= 0)
					return;
				
				label.Text =
					"This is a paragraph of text displayed with " +
					"a FontSize value of ?? that is empirically " +
					"calculated in a loop within the SizeChanged " +
					"handler of the Label's container. This technique " +
					"can be tricky: You don't want to get into " +
					"an infinite loop by triggering a layout pass " +
					"with every calculation. Does it work?";
				
				// Calculate the height of the rendered text.
				FontCalc lowerFontCalc = new FontCalc(label, 10, view.Width);
				FontCalc upperFontCalc = new FontCalc(label, 100, view.Width);

				while (upperFontCalc.FontSize - lowerFontCalc.FontSize > 1)
				{
					// Get the average font size of the upper and lower bounds.
					double fontSize = (lowerFontCalc.FontSize + upperFontCalc.FontSize) / 2;

					// Check the new text height against the container height.
					FontCalc newFontCalc = new FontCalc(label, fontSize, view.Width);
					if (newFontCalc.TextHeight > view.Height) {
						upperFontCalc = newFontCalc;
					}
					else {
						lowerFontCalc = newFontCalc;
					}
				}

				// Set the final font size and the text with the embedded value.
				label.FontSize = lowerFontCalc.FontSize;
				label.Text = label.Text.Replace("??", label.FontSize.ToString("F0"));
			};

			Content = contentView;
		}

		private void buttonLogger ()
		{
			loggerLayout = new StackLayout ();
			Button button = new Button {
				Text = "Log the Click Time"
			};
			button.Clicked += onButtonClicked;

			Padding = new Thickness (5, Device.OnPlatform(20, 0, 0), 5, 0);
			Content = new StackLayout {
				Children = {
					button,
					new ScrollView {
						VerticalOptions = LayoutOptions.FillAndExpand,
						Content = loggerLayout
					}
				}
			};
		}

		private void onButtonClicked (object sender, EventArgs e)
		{
			loggerLayout.Children.Add (new Label {
				Text = "Button clicked at " + DateTime.Now.ToString("T")
			});
		}

		private void twoButtons ()
		{
			loggerLayout = new StackLayout ();

			// Create the Buttons views and attach Clicked handlers.
			addButton = new Button {
				Text = "Add",
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			addButton.Clicked += onButtonClicked2;

			removeButton = new Button
			{
				Text = "Remove",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				IsEnabled = false
			};
			removeButton.Clicked += onButtonClicked2;

			Padding = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 0);

			// Assemble the page.
			Content = new StackLayout {
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							addButton,
							removeButton
						}
					},
					new ScrollView {
						VerticalOptions = LayoutOptions.FillAndExpand,
						Content = loggerLayout
					}
				}
			};
		}

		private void onButtonClicked2 (object sender, EventArgs e)
		{
			Button button = (Button)sender;

			if (button == addButton) {
				// Add Label to scrollable StackLayout
				loggerLayout.Children.Add (new Label {
					Text = "Button clicked at " + DateTime.Now.ToString ("T")
				});
			} else {
				// remove top most Label from StackLayout
				loggerLayout.Children.RemoveAt(0);
			}

			// Enable "Remove" button only if children are present.
			removeButton.IsEnabled = loggerLayout.Children.Count > 0;
		}

		void buttonLambdas ()
		{
			double number = 1;

			Label label = new Label {
				Text = number.ToString(),
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			Button timesButton = new Button {
				Text = "Double",
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)),
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			timesButton.Clicked += (object sender, EventArgs e) => {
				number *= 2;
				label.Text = number.ToString();
			};

			Button divideButton = new Button {
				Text = "Half",
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)),
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			divideButton.Clicked += (object sender, EventArgs e) => {
				number /= 2;
				label.Text = number.ToString();
			};

			Content = new StackLayout {
				Children = {
					label,
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							timesButton,
							divideButton
						}
					}
				}
			};
		}

		private void simplestKeypad () {
			// Create a vertical stack for the entire keypad.
			StackLayout mainStack = new StackLayout {
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center
			};

			// First row is the Label.
			displayLabel = new Label {
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				VerticalOptions = LayoutOptions.Center,
				XAlign = TextAlignment.End
			};
			mainStack.Children.Add (displayLabel);

			// Second row is the backspace Button.
			backspaceButton = new Button {
				Text = "\u21E6",
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)),
				HorizontalOptions = LayoutOptions.Center,
				IsEnabled = false
			};
			backspaceButton.Clicked += onBackspaceButtonClicked;
			mainStack.Children.Add (backspaceButton);

			IDictionary<string, object> properties = Application.Current.Properties;
			if (properties.ContainsKey("displayLabelText")) {
				displayLabel.Text = properties["displayLabelText"] as string;
				backspaceButton.IsEnabled = displayLabel.Text.Length > 0;
			}

			// Now do the 10 number keys.
			StackLayout rowStack = null;
			for (int num = 1; num <= 10; num++) {
				if ((num - 1) % 3 == 0) {
					rowStack = new StackLayout {
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.Center
					};
					mainStack.Children.Add (rowStack);
				}

				Button digitButton = new Button {
					Text = (num % 10).ToString(),
					FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)),
					StyleId = (num % 10).ToString()
				};
				digitButton.Clicked += onDigitButtonClicked;

				// For the zero button, expand to fill horizontally.
				if (num == 10) {
					digitButton.HorizontalOptions = LayoutOptions.FillAndExpand;
				}
				rowStack.Children.Add(digitButton);
			}

			Content = mainStack;
		}

		private void onDigitButtonClicked(object sender, EventArgs args)
		{
			Button button = (Button)sender;
			displayLabel.Text += (string)button.StyleId;
			backspaceButton.IsEnabled = true;
			Application.Current.Properties["displayLabelText"] = displayLabel.Text;
		}

		private void onBackspaceButtonClicked(object sender, EventArgs args)
		{
			string text = displayLabel.Text;
			displayLabel.Text = text.Substring(0, text.Length - 1);
			backspaceButton.IsEnabled = displayLabel.Text.Length > 0;
			Application.Current.Properties["displayLabelText"] = displayLabel.Text;
		}
	}
}