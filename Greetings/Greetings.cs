﻿using System;
using Xamarin.Forms;
using Xamarin.FormsBook.Toolkit;

namespace Greetings {
	public class App : Application {
		const string displayLabelText = "displayLabelText";
		public string DisplayLabelText { set; get; }
		AdderViewModel adderViewModel;

		public App () {
			if (Properties.ContainsKey(displayLabelText)) {
				DisplayLabelText = (string)Properties[displayLabelText];
			}

			Xamarin.FormsBook.Toolkit.Toolkit.Init();
			// The root page of your application
			adderViewModel = new AdderViewModel();
			adderViewModel.RestoreState(Current.Properties);
			MainPage = new EntryFormPage();
		}

		protected override void OnStart () {
			// Handle when your app starts
		}

		protected override void OnSleep () {
			// Handle when your app sleeps
			adderViewModel.SaveState(Current.Properties);
		}

		protected override void OnResume () {
			// Handle when your app resumes
		}
	}
}

