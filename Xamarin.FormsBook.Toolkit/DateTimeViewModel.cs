﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace Xamarin.FormsBook.Toolkit {
	public class DateTimeViewModel : INotifyPropertyChanged {
		DateTime dateTimeModel = DateTime.Now;
		public DateTime DateTimeModel {
			private set {
				if (dateTimeModel != value) {
					dateTimeModel = value;

					// Fire the event.
					PropertyChangedEventHandler handler = PropertyChanged;

					if (handler != null) {
						handler(this, new PropertyChangedEventArgs("DateTimeModel"));
					}
				}
			}

			get {
				return dateTimeModel;
			}
		}
		
		public event PropertyChangedEventHandler PropertyChanged;

		public DateTimeViewModel () {
			Device.StartTimer(TimeSpan.FromMilliseconds (15), OnTimerClick);
		}

		bool OnTimerClick () {
			DateTimeModel = DateTime.Now;
			return true;
		}
	}
}